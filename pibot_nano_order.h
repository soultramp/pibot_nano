#ifndef ORDER_H
#define ORDER_H

// Define the orders that can be sent and received
enum Order {
  START = 0,
  CONNECTED = 1,
  RECEIVED = 2,
  ERROR = 3,
  MOTORS = 4,
  VOLTAGE = 5,
  DISTANCES = 6,
  GET_ENCODERS = 7,
  RESET_ENCODERS  = 8,
  STAT_LED = 9,
  BUZ = 10,
};

typedef enum Order Order;

#endif
